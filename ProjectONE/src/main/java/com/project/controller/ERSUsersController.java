package com.project.controller;

import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.model.ERSUsers;
import com.project.service.ERSUsersService;

import io.javalin.http.Handler;

public class ERSUsersController {
	
	private ERSUsersService uServ;
	ObjectMapper mapper = new ObjectMapper();
	
	public ERSUsersController() {
		
	}
	
	public ERSUsersController(ERSUsersService uServ ) {
		super();
		this.uServ = uServ;
		
	}
	
	public final Handler POSTLOGIN = (ctx) ->{
		ERSUsers user = uServ.verifyPassword(ctx.formParam("username"), ctx.formParam("password"));
		if(user !=null) {
			ctx.sessionAttribute("currentuser", user); 
			ctx.redirect("/html/ershome.html");
		}else {
			ctx.redirect("/html/invalidcredentials.html");
		}
	};
	
	
	public final Handler GETCURRENTUSER = (ctx)->{
		System.out.println((ERSUsers)ctx.sessionAttribute("currentuser"));
		ERSUsers user = uServ.findUserByUserName(((ERSUsers)ctx.sessionAttribute("currentuser")).getErsUserName());
		ctx.json(user);
	};

	
	public final Handler GETREIMBSUBMISSION = (ctx)->{
			int id = Integer.parseInt(ctx.pathParam("id"));
					//System.out.println(id);
			ERSUsers ersusers = uServ.getReimbSubmissionByERSUsersId(id);
			if(ersusers.getErsUsersId() == 0 ) {
				ctx.status(404);
			}else {
				ctx.json(ersusers); 
			}		
	};
	
	
	public final Handler GETALLUSERS = (ctx)->{
		List<ERSUsers> ersusers = uServ.getAllERSUsers();
		if(((ERSUsersService) ersusers ==null)) {
			ctx.status(404);
		}else {
			ctx.status(200);
			ctx.json(ersusers);
		}
	};
	
	
}
