package com.project.controller;

import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.model.ERSReimbursement;
import com.project.model.ERSUsers;
import com.project.service.ERSReimbursementService;
import io.javalin.http.Handler;


public class ERSReimbursementController {

	private ERSReimbursementService rServ;
	ObjectMapper mapper = new ObjectMapper();
	
	public ERSReimbursementController() {
		
	}
	
	public ERSReimbursementController(ERSReimbursementService rServ) {
		super();
		this.rServ = rServ;
	}
	
	public final Handler POSTREIMBSUBMISSION = (ctx)->{ 	// Insert
		ERSReimbursement ersreimb = new ERSReimbursement(Double.parseDouble(ctx.formParam("reimb_amount")), ctx.formParam("reimb_description"), Integer.parseInt(ctx.formParam("reimb_author")), Integer.parseInt(ctx.formParam("reimb_status_id")), Integer.parseInt(ctx.formParam("reimb_type_id")));
		rServ.insertReimbSubmission(ersreimb);
		ctx.status(201);
		ctx.redirect("/html/ershome.html");	
	};
	
	
	public final Handler GETALLREIMBURSEMENTS = (ctx)->{
		 List<ERSReimbursement> ersreimb = rServ.getAllReimbursements();
		if(((ERSReimbursement) ersreimb).getReimbDescription() == null) {
			ctx.status(404);
		}else {
			ctx.status(200);
			ctx.json(ersreimb);
		}
	};
	
	
	public final Handler GETALLREIMBURSEMENTSUNRESOLVED = (ctx)->{
		List<ERSReimbursement> ersreimb = rServ.getReimbursementsByUnResolved();
		if(((ERSReimbursement) ersreimb).getReimbDescription() == null) {
				ctx.status(404);
		}else {
				ctx.status(200);
				ctx.json(ersreimb);
		}
	};
	
	
	public final Handler POSTREIMBURSEMENTSUBMISSIONS = (ctx)->{   // Update
		ERSReimbursement ersreimb = new ERSReimbursement(Integer.parseInt(ctx.formParam("reimb_resolver")), Integer.parseInt(ctx.formParam("reimb_status_id")), Integer.parseInt(ctx.formParam("reimb_id")));
		rServ.insertReimbSubmission(ersreimb);
		ctx.status(201);
		ctx.redirect("/html/ershome.html");	
	};
	
	
	public final Handler GETALLEMPLOYEEREIMBURSEMENTS = (ctx)->{
		//System.out.println("before 1st line");
		ERSUsers ersreimb = rServ.getAllEmployeeDataReimbursements();
		//System.out.println("1st line");
		if(ersreimb.getLastName() == null) {
					ctx.status(404);
		}else {
					ctx.status(200);
					ctx.json(ersreimb);
		}
	};
	
	
	public final Handler GETINDIVIDUALREIMBURSEMENTS = (ctx)->{
		ERSUsers ersreimb = rServ.getIndivEmployeeDataReimbursements(Integer.parseInt(ctx.pathParam("reimb_author")));
		if(ersreimb.getLastName() == null) {
					ctx.status(404);
		}else {
					ctx.status(200);
					ctx.json(ersreimb);
		}
	};
	
	
	public final Handler GETREIMBURSEMENTSBYTYPE = (ctx)->{
		List<ERSReimbursement> ersreimb = rServ.getReimbursementsByType(Integer.parseInt(ctx.pathParam("reimb_type_id")));
		if(((ERSReimbursement) ersreimb).getReimbDescription() == null) {
					ctx.status(404);
		}else {
					ctx.status(200);
					ctx.json(ersreimb);
		}
	};
	
	
	public final Handler GETREIMBURSEMENTAVERAGE = (ctx)->{
		ERSReimbursement ersreimb = rServ.getAverageReimbursements();
		if(ersreimb.getReimbAmount() == 0) {
					ctx.status(404);
		}else {
					ctx.status(200);
					ctx.json(ersreimb);
		}	
	};
	
	
	public final Handler GETHIGHESTREIMBURSEMENT = (ctx)->{
		ERSReimbursement ersreimb = rServ.getHighestReimbursementFigure();
		if(ersreimb.getReimbAmount() == 0) {
					ctx.status(404);
		}else {
					ctx.status(200);
					ctx.json(ersreimb);
		}			
	};
	
	
	public final Handler GETTOTALREIMBURSEMENTOWING = (ctx)->{
		ERSReimbursement ersreimb = rServ.getTotalReimbursementsOwing();
		if(ersreimb.getReimbAmount() == 0) {
					ctx.status(404);
		}else {
					ctx.status(200);
					ctx.json(ersreimb);
		}		
	};
	
	
	public final Handler GETINDIVEMPLOYEETOTALREIMBURSEMENT = (ctx)->{
		ERSReimbursement ersreimb = rServ.getTotalReimbursementsIndivEmployeeOwing(Integer.parseInt(ctx.pathParam("reimb_author")));
		if(ersreimb.getReimbAmount() == 0) {
					ctx.status(404);
		}else {
					ctx.status(200);
					ctx.json(ersreimb);
		}
	};
	
	public final Handler GETEMPLOYEEMOSTMONEYSPENT = (ctx)->{
		//System.out.println("in controller");
		List<ERSReimbursement> ersreimb = rServ.getMostMoneySpentByEmployee();
		//System.out.println(ersreimb);
		if(((ERSReimbursement) ersreimb).getReimbAmount() == 0) {
				ctx.status(404);
		}else {
				ctx.status(200);
				ctx.json(ersreimb); 
		}		
	};
}
