package com.project;

import com.project.controller.ERSReimbursementController;
import com.project.controller.ERSUsersController;
import com.project.dao.ERSDBConnection;
import com.project.dao.ERSReimbursementDAOImpl;
import com.project.dao.ERSUsersDAOImpl;
import com.project.service.ERSReimbursementService;
import com.project.service.ERSUsersService;

import io.javalin.Javalin;

public class AppDriver {
	
	public static void main(String[] args) {
		
		ERSUsersController uCont = new ERSUsersController (new ERSUsersService (new ERSUsersDAOImpl (new ERSDBConnection())));
		ERSReimbursementController rCont = new 	ERSReimbursementController (new ERSReimbursementService (new ERSReimbursementDAOImpl (new ERSDBConnection())));
		
		
		Javalin app = Javalin.create(config ->{
			config.enableCorsForAllOrigins();
			config.addStaticFiles("/frontend");		
			
		});
		
		app.start(9019);
		
		//login
		app.post("/ersystems/login", uCont.POSTLOGIN);
		
		// get current logged in user
		app.get("/ersystems/session", uCont.GETCURRENTUSER);
		
		//[getReimbSubmissionByERSUsersId()] an employee can login to see their own reimbursements
		app.get("ersystems/reimbursement/employee/:id", uCont.GETREIMBSUBMISSION);
		
		
		//[getUserByUserName()] we can search a user by their username
		
		
		// getAll users] from the Employee credentials ONLY
		app.get("/ersystems/reimbursement/manager/allemployeerecords", uCont.GETALLUSERS);
		
		//[insertReimbSubmission()] an employee can submit their own reimbursements using a function 
		app.post("/ersystems/reimbursement/employee/submit", rCont.POSTREIMBSUBMISSION);
		
		//[getAllReimbursements()] a manager can view all reimbursements ONLY past and pending, no employee credentials
		app.get("/ersystems/reimbursement/manager/allreimbrecords", rCont.GETALLREIMBURSEMENTS); // does not work
		
		//getReimbursementsByUnResolved()] a manager can use to see reimbursements that need to be approved or denied
		app.get("/ersystems/reimbursements/manager/open", rCont.GETALLREIMBURSEMENTSUNRESOLVED);
		
		//[updateReimbSubmissionApprvOrDeny()] a manger can approve or deny requests
		app.post("/ersystems/reimbursements/manager/review", rCont.POSTREIMBURSEMENTSUBMISSIONS);
		
		//STATISTICS
		//[getAllEmployeeDataReimbursements()] manager can view all employee data and associated reimbursement submissions
		app.get("/ersystems/reimbursements/allemployeedata", rCont.GETALLEMPLOYEEREIMBURSEMENTS);
		
		//[getIndivEmployeeDataReimbursements()] manager can view individual employee data and associated reimbursement submissions
		app.get("/ersystems/reimbursements/:id", rCont.GETINDIVIDUALREIMBURSEMENTS);
		
		//[getReimbursementsByType()] manager can view total number of submissions by reimbursement types 
		app.get("/ersystems/reimbursements/:type", rCont.GETREIMBURSEMENTSBYTYPE);
		
		//[getAverageReimbursements()] manager can determine the average
		app.get("/ersystems/reimbursements/average", rCont.GETREIMBURSEMENTAVERAGE);
		
		//[getHighestReimbursements()] manager can determine the highest of a single submission
		app.get("/ersystems/reimbursements/higestsum", rCont.GETHIGHESTREIMBURSEMENT);
	
		//[getTotalReimbursements()] manager can determine the total of all submissions to date 
		app.get("/ersystems/reimbursements/total", rCont.GETTOTALREIMBURSEMENTOWING);
		
		//[getMostMoneySpentByEmployee()] manager can determine which employees spend the most money
		app.get("/ersystems/reimbursements/highspending", rCont.GETEMPLOYEEMOSTMONEYSPENT);
		
				
		app.exception(NullPointerException.class, (e, ctx)->{
			ctx.status(404);
			ctx.redirect("/html/invalidcredentials.html");
		});
		
		app.error(404, (ctx)->{ 
			
			ctx.result("404, Resource Not Found");
		});
	}

}
