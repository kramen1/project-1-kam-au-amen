package com.project.dao;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import com.project.model.ERSReimbursement;
import com.project.model.ERSUsers;

public class ERSReimbursementDAOImpl implements ERSReimbursementDAO {

	private ERSDBConnection edbc;
	
	
	 public ERSReimbursementDAOImpl() {
		// TODO Auto-generated constructor stub
	}
	 
	 public ERSReimbursementDAOImpl(ERSDBConnection edbc) {
		 	super();
		 	this.edbc = edbc;
			
	}
	 	
	
	@Override
	public void insertReimbSubmission(ERSReimbursement ersReimb) {
		// TODO Auto-generated method stub
		
		try (Connection con = edbc.getDBConnection()) {
			
			String sql = "{? = call insert_reimbursement(?,?,?,?,?,?)}";
			
			CallableStatement cs = con.prepareCall(sql);
			cs.registerOutParameter(1, Types.VARCHAR);
			cs.setBigDecimal(2, BigDecimal.valueOf(ersReimb.getReimbAmount()));
			cs.setDate(3,null);
			cs.setString(4, ersReimb.getReimbDescription());
			cs.setInt(5, ersReimb.getReimbAuthor());
			cs.setInt(6, ersReimb.getReimbStatusId());
			cs.setInt(7, ersReimb.getReimbTypeId());
			cs.execute();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public List<ERSReimbursement> getAllReimbursements() {
		// TODO Auto-generated method stub
		
		List<ERSReimbursement> reimbList = new ArrayList<>();
		
		try (Connection con = edbc.getDBConnection()) {
			
			String sql = "select * from ers_reimbursement order by reimb_submitted";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
									
			while(rs.next()) {
				reimbList.add(new ERSReimbursement(rs.getInt(1), rs.getDouble(2), rs.getDate(3), rs.getDate(4), rs.getString(5), rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getInt(9)));
						}
						
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return reimbList;

	}

	@Override
	public List<ERSReimbursement> getReimbursementsByUnResolved() {
		// TODO Auto-generated method stub
		
		List<ERSReimbursement> pendingList = new ArrayList<>();
		
		try (Connection con = edbc.getDBConnection()) {
			
			String sql = "select * from ers_reimbursement where reimb_resolver is null order by reimb_submitted";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
									
			while(rs.next()) {
				pendingList.add(new ERSReimbursement(rs.getInt(1), rs.getDouble(2), rs.getDate(3), rs.getDate(4), rs.getString(5), rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getInt(9)));
						}
						
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return pendingList;
		
	}

	@Override
	public void updateReimbSubmissionApprvOrDeny(ERSReimbursement ersReimb) {
		// TODO Auto-generated method stub
		
		try (Connection con = edbc.getDBConnection()) {
			
			String sql = "update ers_reimbursement set reimb_resolved = current_date, reimb_resolver = ?, reimb_status_id = ? where reimb_id = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, ersReimb.getReimbResolver());
			ps.setInt(2, ersReimb.getReimbStatusId());
			ps.setInt(3, ersReimb.getReimbId());
			ps.execute();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public ERSUsers getAllEmployeeDataReimbursements() {
		// TODO Auto-generated method stub
		
		
		try (Connection con = edbc.getDBConnection()) {
			
			String sql = "select * from ers_users eu left outer join ers_reimbursement er on eu.ers_users_id = er.reimb_author order by ers_users_id";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			ERSUsers ersusers = new ERSUsers();	
			List<ERSReimbursement> reimbList = new ArrayList<>();
			
			while(rs.next()) {
				ersusers = new ERSUsers(rs.getInt(1), null, null, rs.getString(4), rs.getString(5), null, rs.getInt(7), null);
				reimbList.add(new ERSReimbursement(rs.getInt(8), rs.getDouble(9), rs.getDate(10), rs.getDate(11), rs.getString(12), rs.getInt(13), rs.getInt(14), rs.getInt(15), rs.getInt(16)));
								
			}
			ersusers.setReimbList(reimbList);
			return ersusers;
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
		
	}

	@Override
	public ERSUsers getIndivEmployeeDataReimbursements(int reimbAuthor) {
		// TODO Auto-generated method stub
		
			try (Connection con = edbc.getDBConnection()) {
			
			String sql = "select * from ers_users eu left outer join ers_reimbursement er on eu.ers_users_id = er.reimb_author where eu.ers_users_id = ? order by reimb_submitted";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, reimbAuthor);
			ResultSet rs = ps.executeQuery();
			ERSUsers ersusers = new ERSUsers();
			List<ERSReimbursement> reimbList = new ArrayList<>();
			
			while(rs.next()) {
				ersusers = new ERSUsers(rs.getInt(1), null, null, rs.getString(4), rs.getString(5), null, rs.getInt(7), null);
				reimbList.add(new ERSReimbursement(rs.getInt(8), rs.getDouble(9), rs.getDate(10), rs.getDate(11), rs.getString(12), rs.getInt(13), rs.getInt(14), rs.getInt(15), rs.getInt(16)));	
			}
			ersusers.setReimbList(reimbList);
			return ersusers;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	return null;
	}

	@Override
	public List<ERSReimbursement> getReimbursementsByType(int reimbTypeId) {
		// TODO Auto-generated method stub
		
		
		try (Connection con = edbc.getDBConnection()) {
			
			String sql = "select * from ers_reimbursement where reimb_type_id = ? order by reimb_submitted";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, reimbTypeId);
			ResultSet rs = ps.executeQuery();
			List<ERSReimbursement> reimbList = new ArrayList<>();
				
			while(rs.next()) {
				reimbList.add(new ERSReimbursement(rs.getInt(1), rs.getDouble(2), rs.getDate(3), rs.getDate(4), rs.getString(5), rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getInt(9)));
			}
			return reimbList;
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	
	@Override
	public ERSReimbursement getAverageReimbursements() {
		// TODO Auto-generated method stub
		
		try (Connection con = edbc.getDBConnection()) {
		
		String sql = "select avg(reimb_amount) as reimb_amount from ers_reimbursement";
		
		PreparedStatement ps = con.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		ERSReimbursement ersravg = new ERSReimbursement();
				
		while(rs.next()) {
			ersravg =	new ERSReimbursement(rs.getDouble(1));
		}
		
		return ersravg;
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
		return null;
	}

	@Override
	public ERSReimbursement getHighestReimbursementFigure() {
		// TODO Auto-generated method stub
		try (Connection con = edbc.getDBConnection()) {
			
			String sql = "select * from ers_reimbursement where reimb_amount in (select max(reimb_amount) from ers_reimbursement)";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			ERSReimbursement ersreimbmax = new ERSReimbursement();
					
			while(rs.next()) {
				ersreimbmax =	new ERSReimbursement(rs.getInt(1), rs.getDouble(2), rs.getDate(3), rs.getDate(4), rs.getString(5), rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getInt(9));
			}
			
			return ersreimbmax;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public ERSReimbursement getTotalReimbursementsOwing() {
		// TODO Auto-generated method stub
		try (Connection con = edbc.getDBConnection()) {
			
			String sql = "select sum(reimb_amount) from ers_reimbursement";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			ERSReimbursement ersrtotal = new ERSReimbursement();
					
			while(rs.next()) {
				ersrtotal =	new ERSReimbursement(rs.getDouble(1));
			}
			
			return ersrtotal;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return null;
	}

	@Override
	public ERSReimbursement getTotalReimbursementsIndivEmployeeOwing(int reimbAuthor) {
		// TODO Auto-generated method stub
		
		try (Connection con = edbc.getDBConnection()) {
			
			String sql = "select reimb_author, sum (reimb_amount) as reimb_amount from ers_reimbursement where reimb_author = ? group by reimb_author";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, reimbAuthor);
			ResultSet rs = ps.executeQuery();
			ERSReimbursement ersremptotl = new ERSReimbursement();
					
			while(rs.next()) {
				ersremptotl =	new ERSReimbursement(rs.getInt(1), rs.getDouble(2));
			}
			
			return ersremptotl;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return null;
	}

	@Override
	public List<ERSReimbursement> getMostMoneySpentByEmployee() {
		// TODO Auto-generated method stub
		System.out.println("in Dao");
		try (Connection con = edbc.getDBConnection()) {
			
			String sql = "select reimb_author, sum (reimb_amount) as reimb_amount from ers_reimbursement group by reimb_author order by reimb_amount desc;";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<ERSReimbursement> ersrspent = new ArrayList<>();
			
			while(rs.next()) {
				ersrspent.add(new ERSReimbursement(rs.getInt(1), rs.getDouble(2)));
			}
			return ersrspent;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

}
