package com.project.dao;

import java.util.List;

import com.project.model.ERSUsers;

public interface ERSUsersDAO {
	
	ERSUsers getReimbSubmissionByERSUsersId (int ersUsersId);
	List<ERSUsers> getAllERSUsers();
	ERSUsers getUserByUserName(String ersUserName);
	
	
}
