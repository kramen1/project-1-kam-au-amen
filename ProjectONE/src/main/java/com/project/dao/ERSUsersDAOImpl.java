package com.project.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import com.project.model.ERSReimbursement;
import com.project.model.ERSUsers;

public class ERSUsersDAOImpl implements ERSUsersDAO {

	private ERSDBConnection edbc;
	
	
	 public ERSUsersDAOImpl() {
		// TODO Auto-generated constructor stub
	}
	 
	 public ERSUsersDAOImpl(ERSDBConnection edbc) {
		 	super();
		 	this.edbc = edbc;
			
	}
	 
	 
	@Override
	public ERSUsers getReimbSubmissionByERSUsersId(int ersUsersId) {
		// TODO Auto-generated method stub
		
		try (Connection con = edbc.getDBConnection()) {
			
			String sql = "select * from ers_users eu left outer join ers_reimbursement er on er.reimb_author = eu.ers_users_id where reimb_author = ?";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, ersUsersId);
			ResultSet rs = ps.executeQuery();
			ERSUsers ersusers = new ERSUsers();
			List<ERSReimbursement> reimbList = new ArrayList<>();
			
			while(rs.next()) {
				ersusers = new ERSUsers(rs.getInt(1), null, null, rs.getString(4), rs.getString(5), null, rs.getInt(7), null) ;
				reimbList.add(new ERSReimbursement(rs.getDouble(9), rs.getDate(10), rs.getDate(11), rs.getString(12), rs.getInt(13), rs.getInt(14), rs.getInt(15), rs.getInt(16)));
			}
			ersusers.setReimbList(reimbList);
			return ersusers;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public List<ERSUsers> getAllERSUsers() {
		// TODO Auto-generated method stub
		
		
		try (Connection con = edbc.getDBConnection()) {
			
			String sql = "select * from ers_users order by ers_users_id";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<ERSUsers> usersList = new ArrayList<>();
									
			while(rs.next()) {
				usersList.add(new ERSUsers(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7), null));
			}
			return usersList;
						
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	@Override
		public ERSUsers getUserByUserName(String ersUserName) {
		
		try(Connection con =  edbc.getDBConnection()){
			
			String sql = "select * from ers_users where ers_username = ?";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, ersUserName);
			ResultSet rs = ps.executeQuery();
			ERSUsers ersusers = null;
			
			while(rs.next()) {
				ersusers = new ERSUsers(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7), null);
			}
			
			return ersusers;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	
}
