package com.project.dao;

import java.util.List;

import com.project.model.ERSReimbursement;
import com.project.model.ERSUsers;

public interface ERSReimbursementDAO {

	
	void insertReimbSubmission(ERSReimbursement ersReimb);
	List<ERSReimbursement> getAllReimbursements();
	List<ERSReimbursement> getReimbursementsByUnResolved();
	void updateReimbSubmissionApprvOrDeny(ERSReimbursement ersReimb);
	ERSUsers getAllEmployeeDataReimbursements ();
	ERSUsers getIndivEmployeeDataReimbursements (int reimbAuthor);
	List<ERSReimbursement> getReimbursementsByType(int reimbTypeId);
	ERSReimbursement getAverageReimbursements ();
	ERSReimbursement getHighestReimbursementFigure ();
	ERSReimbursement getTotalReimbursementsOwing ();
	ERSReimbursement getTotalReimbursementsIndivEmployeeOwing (int reimbAuthor);
	List<ERSReimbursement> getMostMoneySpentByEmployee ();
	//ERSReimbursement getMostMoneySpentByEmployee ();

	
}
