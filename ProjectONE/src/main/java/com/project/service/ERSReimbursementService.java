package com.project.service;

import java.util.List;

import com.project.dao.ERSReimbursementDAOImpl;
import com.project.model.ERSReimbursement;
import com.project.model.ERSUsers;


public class ERSReimbursementService {
	
	private ERSReimbursementDAOImpl rDao;
	
	public ERSReimbursementService() {
		
	}

	
	public ERSReimbursementService(ERSReimbursementDAOImpl rDao) {
		super();
		this.rDao = rDao;
	}
	
	
	public void insertReimbSubmission(ERSReimbursement ersReimb) {
			rDao.insertReimbSubmission(ersReimb);
	}
	
	
	public List<ERSReimbursement> getAllReimbursements() {
		return rDao.getAllReimbursements();
	}
	
	
	public List<ERSReimbursement> getReimbursementsByUnResolved() {
		return rDao.getReimbursementsByUnResolved();
	}
	
	
	public void updateReimbSubmissionApprvOrDeny(ERSReimbursement ersReimb) {
			rDao.updateReimbSubmissionApprvOrDeny(ersReimb);
	}
	

	public ERSUsers getAllEmployeeDataReimbursements() {
		return rDao.getAllEmployeeDataReimbursements();
	}
	
	
	public ERSUsers getIndivEmployeeDataReimbursements(int reimbAuthor) {
		return rDao.getIndivEmployeeDataReimbursements(reimbAuthor);
	}
	
	
	public List<ERSReimbursement> getReimbursementsByType(int reimbTypeId) {
		return rDao.getReimbursementsByType(reimbTypeId);
	}
	
	
	public ERSReimbursement getAverageReimbursements() {
		return rDao.getAverageReimbursements();
	}
	
	
	public ERSReimbursement getHighestReimbursementFigure() {
		return rDao.getHighestReimbursementFigure();
	}
	
	
	public ERSReimbursement getTotalReimbursementsOwing() {
		return rDao.getTotalReimbursementsOwing();
	}
	
	
	public ERSReimbursement getTotalReimbursementsIndivEmployeeOwing(int reimbAuthor) {
		return rDao.getTotalReimbursementsIndivEmployeeOwing(reimbAuthor);
	}
	
	
	public List<ERSReimbursement> getMostMoneySpentByEmployee() {
		//System.out.println("in service");
		return rDao.getMostMoneySpentByEmployee();
	}
	
 }
