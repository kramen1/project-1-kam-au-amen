package com.project.service;

import java.util.List;

import com.project.dao.ERSUsersDAOImpl;
import com.project.model.ERSUsers;

public class ERSUsersService {
	
	private ERSUsersDAOImpl uDao;
	
	public ERSUsersService() {
		
	}
	
	public ERSUsersService(ERSUsersDAOImpl uDao) {
		super();
		this.uDao = uDao;
	}
	
	
	public ERSUsers getReimbSubmissionByERSUsersId(int ersUsersId) {
		return uDao.getReimbSubmissionByERSUsersId(ersUsersId);
	}
	
	
	public List<ERSUsers> getAllERSUsers() {
		return uDao.getAllERSUsers();
		}

	public ERSUsers findUserByUserName(String ersUserName) {
		ERSUsers user = uDao.getUserByUserName (ersUserName);
		if (user==null) {
			throw new NullPointerException();
		}
		return user;
	}
	
	public ERSUsers verifyPassword(String ersUserName, String password) {
		ERSUsers user = findUserByUserName(ersUserName);
		if (user.getPassword().equals(password)) {
			return user;
		}
		return null;	
	}
	
 }
