package com.project;

import com.project.dao.ERSDBConnection;
import com.project.dao.ERSReimbursementDAOImpl;
import com.project.dao.ERSUsersDAOImpl;
import com.project.model.ERSReimbursement;

import io.javalin.Javalin;


public class MainDriver {
	
	public static void main(String[] args) {
		
		ERSDBConnection con = new ERSDBConnection();
		ERSUsersDAOImpl uDao = new ERSUsersDAOImpl(con);
		ERSReimbursementDAOImpl rDao = new ERSReimbursementDAOImpl(con);
		

		
				//uDao.getReimbSubmissionByERSUsersId(5); // Works
				//System.out.println(uDao.getReimbSubmissionByERSUsersId(5));
				//		
				//uDao.getAllERSUsers(); //Works
				//System.out.println(uDao.getAllERSUsers());
//		
				//rDao.insertReimbSubmission(new ERSReimbursement(1000, null, null, "New office coffee machine", 6, 1, 4, 4));
//				//No output // Works
//		
				//rDao.getAllReimbursements(); //Works
				//System.out.println(rDao.getAllReimbursements());
//		
				//rDao.getReimbursementsByUnResolved(); // Works
				//System.out.println(rDao.getReimbursementsByUnResolved());
//		
				//rDao.updateReimbSubmissionApprvOrDeny(new ERSReimbursement(null, 2, 2, 6));
				//No output // WORKS
//		
				//rDao.getAllEmployeeDataReimbursements(); // WORKS
				//System.out.println(rDao.getAllEmployeeDataReimbursements());
//		
				//rDao.getIndivEmployeeDataReimbursements(4); //WORKS
				//System.out.println(rDao.getIndivEmployeeDataReimbursements(4));
//		
				//rDao.getReimbursementsByType(2); //WORKS
				//System.out.println(rDao.getReimbursementsByType(2));
//		
				//rDao.getAverageReimbursements(); //WORKS
				//System.out.println(rDao.getAverageReimbursements());
//		
				//rDao.getHighestReimbursementFigure(); //WORKS
				//System.out.println(rDao.getHighestReimbursementFigure());
//		
				//rDao.getTotalReimbursementsOwing(); //Works
				//System.out.println(rDao.getTotalReimbursementsOwing());
//		
				//rDao.getTotalReimbursementsIndivEmployeeOwing(5); // WORKS
				//System.out.println(rDao.getTotalReimbursementsIndivEmployeeOwing(5));
//		
		//rDao.getMostMoneySpentByEmployee(); //NOT - not listing all 5
		//System.out.println(rDao.getMostMoneySpentByEmployee());
//		
//		
		
			
	}

}
