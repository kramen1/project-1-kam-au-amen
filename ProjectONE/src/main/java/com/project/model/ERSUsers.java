package com.project.model;

import java.util.List;

public class ERSUsers {
	
	private int ersUsersId;
	private String ersUserName;
	private String password;
	private String firstName;
	private String lastName;
	private String email;
	private int roleId;
	private List<ERSReimbursement> reimbList;
	
	
	public ERSUsers() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public ERSUsers(String ersUserName, String password, String firstName, String lastName,
			String email, int roleId, List<ERSReimbursement> reimbList) {
		super();
		this.ersUserName = ersUserName;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.roleId = roleId;
		this.reimbList = reimbList;
	}
	
	
	public ERSUsers(int ersUsersId, String ersUserName, String password, String firstName, String lastName,
			String email, int roleId, List<ERSReimbursement> reimbList) {
		super();
		this.ersUsersId = ersUsersId;
		this.ersUserName = ersUserName;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.roleId = roleId;
		this.reimbList = reimbList;
	}


	public String getErsUserName() {
		return ersUserName;
	}


	public void setErsUserName(String ersUserName) {
		this.ersUserName = ersUserName;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public int getRoleId() {
		return roleId;
	}


	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}


	public List<ERSReimbursement> getReimbList() {
		return reimbList;
	}


	public void setReimbList(List<ERSReimbursement> reimbList) {
		this.reimbList = reimbList;
	}


	public int getErsUsersId() {
		return ersUsersId;
	}


	@Override
	public String toString() {
		return "ERSUsers [ersUsersId=" + ersUsersId + ", ersUserName=" + ersUserName + ", password=" + password
				+ ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", roleId=" + roleId
				+ ", reimbList=" + reimbList + "]";
	}
	
	
	
}
