package com.project.eval;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.when;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.sql.Connection;

import org.eclipse.jetty.server.Authentication.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.project.dao.ERSDBConnection;
import com.project.dao.ERSReimbursementDAOImpl;
import com.project.dao.ERSUsersDAOImpl;
import com.project.model.ERSReimbursement;
import com.project.model.ERSUsers;

public class UserDAOTest {
	
	
@Mock
private ERSDBConnection dbcon;


@Mock
private Connection con;

@Mock
private PreparedStatement ps;

@Mock
private CallableStatement cs;

@Mock
private ResultSet rs;


private ERSUsersDAOImpl uDao;

private ERSReimbursementDAOImpl rDao;


private ERSUsers user;

private List<ERSReimbursement> reimbList;

@BeforeEach
public void setUp() throws Exception{
//int ersUsersId, String ersUserName, String password, String firstName, String lastName,
//			String email, int roleId, List<ERSReimbursement> reimbList) {
   
    user = new ERSUsers(1, "Kmau", "password", "K", "M", "kmau@aol.com", 1, reimbList);

    MockitoAnnotations.initMocks(this);
    uDao = new ERSUsersDAOImpl(dbcon);
    when(dbcon.getDBConnection()).thenReturn(con);
    when(con.prepareStatement(isA(String.class))).thenReturn(ps);
    when(con.prepareCall(isA(String.class))).thenReturn(cs);
    when(ps.executeQuery()).thenReturn(rs);
    when(ps.execute()).thenReturn(true);
    when(cs.execute()).thenReturn(true);
    when(cs.getString(isA(Integer.class))).thenReturn("User Test Successful!!!");
    when(rs.next()).thenReturn(true).thenReturn(false);
    when(rs.getInt(1)).thenReturn(user.getErsUsersId());
    when(rs.getString(2)).thenReturn(user.getErsUserName());
    when(rs.getString(3)).thenReturn(user.getPassword());
    when(rs.getString(4)).thenReturn(user.getFirstName());
    when(rs.getString(5)).thenReturn(user.getLastName());
    when(rs.getString(6)).thenReturn(user.getEmail());
    when(rs.getInt(7)).thenReturn(user.getRoleId());
    when(rs.getObject(8)).thenReturn(user.getReimbList());
}


	@Test
	public void getReimbSubmissionByERSUsersIdTest() throws Exception{
		uDao.getReimbSubmissionByERSUsersId(1);
		assertEquals(user.getErsUsersId(), user.getErsUsersId());
		
	}
	
	@Test
	public void getUserByUserNameTest() throws Exception{
		ERSUsers testUser = uDao.getUserByUserName("Kmau");
		assertEquals(user.getErsUsersId(), testUser.getErsUsersId());
		assertEquals(user.getErsUserName(), testUser.getErsUserName());
		assertEquals(user.getPassword(), testUser.getPassword());
		assertEquals(user.getFirstName(), testUser.getFirstName());
		assertEquals(user.getLastName(), testUser.getLastName());
		assertEquals(user.getEmail(), testUser.getEmail());
		assertEquals(user.getReimbList(), testUser.getReimbList());
	}
	
	@Test
	public void getAllERSUsers(){
		List<ERSUsers> reimbList = uDao.getAllERSUsers();
		ERSUsers rList1 = reimbList.get(0);
		ERSUsers rList2 = reimbList.get(0);
		assertEquals(rList1.getReimbList(), rList2.getReimbList());	
		
	}
	
}





